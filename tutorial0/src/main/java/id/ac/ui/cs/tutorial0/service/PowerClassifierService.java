package id.ac.ui.cs.tutorial0.service;

public interface PowerClassifierService {
    public String classifyPowerLevel(int power);
}
