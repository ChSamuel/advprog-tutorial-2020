package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

@Service
public class PowerClassifierServiceImpl implements PowerClassifierService{
    @Override
    public String classifyPowerLevel(int power) {
        if(power > 100000) return "A Class";
        else if(power > 20000) return "B Class";
        return "C Class";
    }
}
