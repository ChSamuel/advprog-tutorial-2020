package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
        }

        public void update(){
                if(guild.getQuestType().equals("E") || guild.getQuestType().equals("D")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
