package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack(){
        return "Menyerang menggunakan Sihir";
    }

    public String getType(){
        return "Magic";
    }
}
