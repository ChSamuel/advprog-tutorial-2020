package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private List<Member> underlings = new ArrayList<Member>();
    private String name;
    private String role;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if((name.equals("Heathcliff") && role.equals("Master")) || underlings.size() < 3){
            underlings.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        underlings.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return underlings;
    }
}
