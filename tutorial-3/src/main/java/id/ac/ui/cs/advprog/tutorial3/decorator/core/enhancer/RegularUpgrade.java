package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {
    int upgradeValue;
    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random rand = new Random();
        upgradeValue = 1+ rand.nextInt(4);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        return upgradeValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return "Regular " + weapon.getDescription();
    }
}
