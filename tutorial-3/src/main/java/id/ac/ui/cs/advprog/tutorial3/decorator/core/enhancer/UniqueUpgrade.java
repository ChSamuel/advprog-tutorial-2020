package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {
    int upgradeValue;
    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
        Random rand = new Random();
        upgradeValue = 10 + rand.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        return upgradeValue + weapon.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return "Unique " + weapon.getDescription();
    }
}
