package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    private String name;
    private String role;
    private List<Member> underlings = new ArrayList<Member>();

    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        // Do nothing, because OrdinaryMembers don't have underlings
    }

    @Override
    public void removeChildMember(Member member) {
        // Do nothing, because OrdinaryMembers don't have underlings
    }

    @Override
    public List<Member> getChildMembers() {
        return underlings;
    }
}
