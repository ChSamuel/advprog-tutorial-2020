package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private static HolyWish instance = null;
    private String wish;

    // Singleton approach yang digunakan : lazy
    // Instance class ini hanya akan dibuat saat pemanggilan
    // pertama getInstance method, selebihnya hanya
    // return instance

    private HolyWish(){}

    public static HolyWish getInstance(){
        if(instance == null){
            instance = new HolyWish();
        }
        return instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
