package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testProduceKnightAndGetKnight(){
        academyService.produceKnight("Lordran", "majestic");
        Knight knight = academyService.getKnight();
        knight.setName("Dummy1");
        assertEquals("Dummy1", knight.getName());

        academyService.produceKnight("Drangleic", "metal cluster");
        knight = academyService.getKnight();
        knight.setName("Dummy2");
        assertEquals("Dummy2", knight.getName());
    }

    @Test
    public void testGetKnightAcademies(){
        List academies = academyService.getKnightAcademies();
        assertTrue(academies.get(0) instanceof LordranAcademy);
        assertTrue(academies.get(1) instanceof DrangleicAcademy);
    }
}
