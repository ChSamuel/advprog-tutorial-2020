package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    HolyGrail holyGrail;
    HolyWish holyWish;

    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
        holyWish = holyGrail.getHolyWish();
    }

    @Test
    public void testMakeAWish(){
        holyGrail.makeAWish("Win");
        assertEquals("Win", holyWish.getWish());
    }

    @Test
    public void testGetHolyWish(){
        assertEquals(holyWish, holyGrail.getHolyWish());
    }
}
