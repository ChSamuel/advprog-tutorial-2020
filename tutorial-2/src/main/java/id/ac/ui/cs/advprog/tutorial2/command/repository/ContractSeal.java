package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Spell latestSpell;
    private boolean undone = true;
    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        undone = false;
        Spell temp = spells.get(spellName);
        latestSpell = temp;
        latestSpell.cast();
    }

    public void undoSpell() {
        if(undone == false) {
            latestSpell.undo();
            undone = true;
        }
        else if(latestSpell instanceof BlankSpell){
            latestSpell.undo();
            undone = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
